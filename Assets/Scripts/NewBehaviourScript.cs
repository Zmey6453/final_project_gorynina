﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{


    public Transform cube;

    public float sensitivityX;
    public float sensitivityY;
    GameObject pivot;

    void Start()
    {
        pivot = new GameObject("pivot");
        pivot.transform.position = cube.position;
        transform.SetParent(pivot.transform);
    }

    void Update()
    {
        pivot.transform.eulerAngles += new Vector3(Input.GetAxis("Mouse Y") * -sensitivityX * Time.deltaTime, Input.GetAxis("Mouse X") * sensitivityY * Time.deltaTime, 0);
    }
}
