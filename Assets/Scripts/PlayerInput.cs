﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class PlayerInput
{
    internal Vector3 axis = Vector3.zero;
    Vector2 touch_dist;
    Vector2 start_touch_position;
    bool pressed;
    bool init_touch;

    float max_distance;
    GameObject base_img;
    GameObject stick_img;

    public void Init(GameObject gamepadBase, GameObject gamepadStick)
    {
        base_img = gamepadBase;
        stick_img = gamepadStick;
        max_distance = 31;
    }

    public void Tick()
    {
        axis.x = Input.GetAxisRaw("Horizontal");
        axis.z = Input.GetAxisRaw("Vertical");

        pressed = Input.GetMouseButton(0);

        if (base_img != null && stick_img != null)
        {
            base_img.gameObject.SetActive(pressed);
            stick_img.gameObject.SetActive(pressed);
        }

        if (pressed)
        {
            if (!init_touch)
            {
                start_touch_position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
                init_touch = true;
                if (base_img != null)
                    base_img.transform.position = new Vector3(start_touch_position.x, start_touch_position.y, 0.0f);
            }

            touch_dist = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - start_touch_position;

            var distance = Mathf.Min(touch_dist.magnitude, max_distance);
            touch_dist = touch_dist.normalized * distance;
            touch_dist.x = touch_dist.x / max_distance;
            touch_dist.y = touch_dist.y / max_distance;

            axis.x = touch_dist.x;
            axis.z = touch_dist.y;

            if (stick_img != null)
                stick_img.transform.localPosition = new Vector3(axis.x * max_distance, axis.z * max_distance, 0.0f);
        }
        else
        {
            touch_dist = Vector2.zero;
            init_touch = false;
        }

        if (Mathf.Abs(axis.x) > 0.5f && Mathf.Abs(axis.z) < 0.5f)
            axis.Scale(Vector3.right);
        else if (Mathf.Abs(axis.z) > 0.5f && Mathf.Abs(axis.x) < 0.5f)
            axis.Scale(Vector3.forward);
    }
}
