﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JoystickController : MonoBehaviour
{

    public GameObject stick;
    public PlayerFollower camera;
    public Player player;
     float maxDistStick = 200;
     float sizeSensetive = 230;
     float maxSize = 600;
    Vector3 tаrget_vector;
    Vector3 tаrget_camera_vector;
    Dictionary<int, bool> fingers = new Dictionary<int, bool>();
    List<int> finger_ids = new List<int>();
    //int buttonUFingerId = -1;
    //int buttonDFingerId = -1;
    //int joystickFingerId = -1;
    //bool updateJoystick;
    //bool updateButtonU;
    //bool updateButtonD;
    Vector2 touchDist;
    public Button buttonU;
    public Button buttonD;
    Rect ButtonURect;
    Rect ButtonDRect;
   // public Transform cube;

    public float sensitivityX;
    public float sensitivityY;
    GameObject pivot;

    void Start()
    {
        stick.transform.position = transform.position;
        camera = FindObjectOfType<PlayerFollower>();
        //maxDistStick = joystickMaxDistance();

        ButtonURect = buttonU.GetComponent<RectTransform>().rect;
        ButtonDRect = buttonD.GetComponent<RectTransform>().rect;
    }

    float joystickMaxDistance()
    {
            return GetComponent<RectTransform>().rect.width / 2.0f - stick.GetComponent<RectTransform>().rect.width / 2.0f;
 
    }
    //float getmaxSize()
    //{
    //    return GetComponent<RectTransform>().rect.width / 2.0f - ButtonURect.width / 2.0f;

    //}
    void Update()
    {
        //if (Input.anyKey)
         //   SetVirtualJoystickActive(false);

       // UpdateUnityInput();

        //UpdateVirtualInput();
        UpdateMouse();
    }


    //void UpdateButtonU(bool pressed, Vector3 touchPosition, int fingerId)
    //{
    //    if (updateButtonU)
    //        return;
    //
    //    if (!IsTouch(pressed, touchPosition, fingerId, buttonU.transform.position, buttonDFingerId, ButtonURect.width / 2.0f))
    //        return;
    //
    //    buttonUFingerId = fingerId;
    //
    //    updateButtonU = true;
    //
    //    if (!pressed)
    //        buttonUFingerId = -1;
    //
    //   // buttonA.transform.localScale = Vector3.one * (pressA ? 0.8f : 1.0f);
    //}
    //void UpdateButtonD(bool pressed, Vector3 touchPosition, int fingerId)
    //{
    //    if (updateButtonU)
    //        return;
    //
    //    if (!IsTouch(pressed, touchPosition, fingerId, buttonD.transform.position, buttonDFingerId, ButtonDRect.width / 2.0f))
    //        return;
    //
    //    buttonDFingerId = fingerId;
    //
    //    updateButtonD = true;
    //
    //    if (!pressed)
    //        buttonDFingerId = -1;
    //
    //}
    //
    //void UpdateVirtualInput()
    //{
    //    foreach (var key in finger_ids)
    //        fingers[key] = false;
    //
    //    for (int i = 0; i < Input.touchCount; ++i)
    //    {
    //        var touch = Input.GetTouch(i);
    //
    //        if (fingers.ContainsKey(touch.fingerId))
    //            finger_ids.Add(touch.fingerId);
    //        fingers[touch.fingerId] = true;
    //
    //        var pressed = touch.phase == TouchPhase.Began ||
    //          touch.phase == TouchPhase.Moved ||
    //          touch.phase == TouchPhase.Stationary;
    //        // var position = touch.position;
    //        Vector3 touch_pos = touch.position;
    //        UpdateJoystick(pressed, touch_pos, touch.fingerId);
    //        UpdateButtonU(pressed, touch_pos, touch.fingerId);
    //        UpdateButtonD(pressed, touch_pos, touch.fingerId);
    //    }
    //
    //    foreach (var finger in fingers)
    //    {
    //        if (!finger.Value)
    //        {
    //            UpdateJoystick(false, Vector3.zero, finger.Key);
    //            UpdateButtonU(false, Vector3.zero, finger.Key);
    //            UpdateButtonD(false, Vector3.zero, finger.Key);
    //        }
    //    }
    //
    //    updateJoystick = false;
    //    updateButtonU = false;
    //    //updateButtonA = false;
    //}
    //
//
    //void UpdateJoystick(bool pressed, Vector3 touch_pos, int fingerId)
    //{
    //    if (updateJoystick)
    //        return;
    //
    //    if (!IsTouch(pressed, touch_pos, fingerId, transform.position, joystickFingerId, maxDistStick))
    //        return;
    //
    //    joystickFingerId = fingerId;
    //
    //    updateJoystick = true;
    //
    //    if (pressed)
    //    {
    //        //touchDist = touch_pos - transform.position;
    //
    //        //var distance = Mathf.Min(touchDist.magnitude, maxDistStick);
    //        //touchDist = touchDist.normalized * distance;
    //        //touchDist.x = touchDist.x / maxDistStick;
    //        //touchDist.y = touchDist.y / maxDistStick;
//
    //        //horizontal = touchDist.x;
    //        //vertical = touchDist.y;
    //
    //       
    //            stick.transform.position = touch_pos;
    //            tаrget_vector.z = tаrget_vector.y;
    //            tаrget_vector.y = 0;
    //            player.MovingGorizontal(tаrget_vector.normalized);
     //                    
    //    }
    //    else
    //    {
    //        touchDist = Vector2.zero;
    //        joystickFingerId = -1;
    //    }
    //
    //    //stick.transform.localPosition = new Vector3(touchDist.x * maxDistStick, touchDist.y * maxDistStick, 0.0f);
    //
    //   //player.MovingGorizontal(new Vector3(touchDist.x * maxDistStick, touchDist.y * maxDistStick, 0.0f).normalized);
    //}
    //
    //bool IsTouch(bool pressed, Vector3 touchPosition, int fingerId, Vector3 controllPosition, int controllFingerId, float safeTouchDistance)
    //{
    //    var touchDistance = (touchPosition - controllPosition).magnitude;
    //
    //    if (controllFingerId >= 0 && fingerId != controllFingerId)
    //        return false;

    //    if (controllFingerId < 0 && touchDistance > safeTouchDistance)
    //        return false;

    //    return true;
    //}


    void UpdateMouse()
    {
        if (Input.GetMouseButton(0))
        {
            //if (stick.transform.position != transform.position) return;
            Vector3 touch_pos = Input.mousePosition;


            tаrget_vector = touch_pos - transform.position;

            // Debug.Log(maxDistStick);
            //Debug.Log(tаrget_vector.magnitude);
            if (tаrget_vector.magnitude <= maxDistStick)
            {

                stick.transform.position = touch_pos;
                tаrget_vector.z = tаrget_vector.y;
                tаrget_vector.y = 0;
                player.MovingGorizontal(tаrget_vector.normalized);

            }
            else if (tаrget_vector.magnitude > sizeSensetive && tаrget_vector.magnitude < maxSize)
            {
                //Debug.Log(Input.mousePosition);
                //Debug.Log(touch_pos);
                //tаrget_camera_vector = Input.mousePosition - touch_pos;
                //Debug.Log(tаrget_camera_vector.magnitude);
                //camera.CameraRotate(tаrget_camera_vector.magnitude);
                //pivot = new GameObject("pivot");
                //pivot.transform.position = player.transform.position;
                //transform.SetParent(pivot.transform);
                //pivot.transform.eulerAngles += new Vector3(Input.GetAxis("Mouse Y") * -sensitivityX * Time.deltaTime, Input.GetAxis("Mouse X") * sensitivityY * Time.deltaTime, 0);
                //camera.sensitivityX = 50;

                //camera.AxisY = Input.GetAxis("Mouse Y");
                ////Debug.Log(Input.GetAxis("Mouse Y"));
                //camera.AxisX = Input.GetAxis("Mouse X");
                ////Debug.Log(Input.GetAxis("Mouse X"));
                //camera.CameraRotate();

                return;
            }
            else
            {
                //tаrget_vector = stick.transform.position - transform.position;
                //tаrget_vector.z = tаrget_vector.y;
                //tаrget_vector.y = 0;
                //player.MovingGorizontal(tаrget_vector.normalized);
                //Debug.Log(tаrget_vector.magnitude);
            }

        }
        else
        {
            stick.transform.position = transform.position;
            player.MovingGorizontal(Vector3.zero);
        }
    }



    //Debug.Log(Input.touches.Length);
    //foreach (Touch touch in Input.touches)
    //{
    //    Debug.Log("ok");
    //    Vector3 touch_pos = touch.position;
    //    Vector3 touch_pos_end = Vector3.zero; 
    //    tаrget_vector = touch_pos - transform.position;
    //    Debug.Log(tаrget_vector);
    //    if (tаrget_vector.magnitude <= maxDistStick)
    //    {
    //        stick.transform.position = touch_pos;
    //        tаrget_vector.z = tаrget_vector.y;
    //        tаrget_vector.y = 0;
    //        player.MovingGorizontal(tаrget_vector.normalized);

    //    }
    //    else if ((tаrget_vector.magnitude >= sizeSensetive) && (tаrget_vector.magnitude <= maxSize))
    //    {
    //        if (touch.phase == TouchPhase.Began)
    //        {
    //            touch_pos = touch.position;
    //        }

    //        if (touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Ended)
    //        {
    //            touch_pos_end = touch.position;
    //        }



    //        Debug.Log(touch_pos);
    //        Debug.Log(touch_pos_end);
    //        //tаrget_camera_vector = Input.mousePosition - touch_pos;
    //        //Debug.Log(tаrget_camera_vector.magnitude);
    //        //camera.CameraRotate(tаrget_camera_vector.magnitude);
    //    }
    //}
       //}
}