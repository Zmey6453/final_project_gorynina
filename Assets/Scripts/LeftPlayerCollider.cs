﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftPlayerCollider : MonoBehaviour
{
    public Player player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Untagged")) player.nearObjectLeft = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Untagged")) player.nearObjectLeft = false;
    }
}
