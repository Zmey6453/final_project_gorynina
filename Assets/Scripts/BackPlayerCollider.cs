﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackPlayerCollider : MonoBehaviour
{
    public Player player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Untagged")) player.nearObjectBack = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Untagged")) player.nearObjectBack = false;
    }
}
