﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrontPlayerCollider : MonoBehaviour
{
    public Player player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Untagged")) player.nearObjectFront = true;

    }
    private void  OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Untagged")) player.nearObjectFront = false;
    }
}
