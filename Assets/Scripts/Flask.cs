﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flask : MonoBehaviour
{
    public RimpsPillar pillar;
    Color selfColor;
    int colorComponent;
    public GameObject coloredChild;
    bool first = true;

    void Start()
    {
        //pillar = GetComponent<RimpsPillar>();//не работает 
        pillar = FindObjectOfType<RimpsPillar>();
        //if (pillar) Debug.Log("oj");
        colorComponent = Random.Range(1, 7);
        //Debug.Log(colorComponent);
        switch (colorComponent)
        {
            case 1:
                {
                    selfColor = new Color(Random.Range(1, 100), 0, 0);//red
                    //Debug.Log(selfColor);
                    break;
                }
            case 2:
                {
                    selfColor = new Color(0,Random.Range(1, 100), 0);//green                    
                    //Debug.Log(selfColor);
                    break;
                }
            case 3:
                {
                    selfColor = new Color(0,0,Random.Range(1, 100));//blue                    
                    //Debug.Log(selfColor);
                    break;
                }
            case 4:
                {
                    selfColor = new Color(0, Random.Range(1, 50), Random.Range(1, 50));//cyan
                    //Debug.Log(selfColor);
                    break;
                }
            case 5:
                {
                    selfColor = new Color(Random.Range(1, 50), 0, Random.Range(1, 50));//magenta
                    //Debug.Log(selfColor);
                    break;
                }
            case 6:
                {
                    selfColor = new Color(Random.Range(1, 50), Random.Range(1, 50), 0);//yellow
                    //Debug.Log(selfColor);
                    break;
                }

        }
        coloredChild.GetComponent<Renderer>().material.color = selfColor;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!pillar) pillar = FindObjectOfType<RimpsPillar>();

        pillar.Flask(first, selfColor);
        first = false;
        Destroy(this.gameObject);
            
    }
}
