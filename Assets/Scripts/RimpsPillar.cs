﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class RimpsPillar : MonoBehaviour
{

    List<Material> levelsObjects = new List<Material>();
    int i;
    int flaskCount = 15;
    public bool wasEaten = false;

    public GameObject level;
    void Start()
    {
        Application.targetFrameRate = 60;
        foreach (Transform child in level.transform) levelsObjects.Add(child.GetComponent<Renderer>().material);
    }

    void OnGUI()
    {
        if (wasEaten)
        {
            GUI.BeginGroup(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 50, 120, 60));
            GUI.Box(new Rect(0, 0, 120, 60), "Вы стали обедом");
            if (GUI.Button(new Rect(10, 20, 100, 30), "Играть"))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            GUI.EndGroup();
        }
        if (flaskCount == 0)
        {
            GUI.BeginGroup(new Rect(Screen.width / 2 - 50, Screen.height / 2 - 50, 120, 60));
            GUI.Box(new Rect(0, 0, 120, 60), "Вы стали героем");
            if (GUI.Button(new Rect(10, 20, 100, 30), "Играть"))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            GUI.EndGroup();
        }
    }

    public void Flask(bool first, Color addition)
    {
        if (first)
        {
            flaskCount--;
            Debug.Log(flaskCount);
            int j;
            for (int k = 0; k < 10; k++)
            {
                j = Random.Range(0, levelsObjects.Count);
                i = Random.Range(0, levelsObjects.Count);
                levelsObjects[i].color -= addition;
                if (levelsObjects[i].color == Color.black) levelsObjects[i].color = new Color(0.0f, 100.0f, 100.0f, 255.0f);
                while (i == j) j = Random.Range(0, levelsObjects.Count);
                levelsObjects[j].color += addition;
            }
        }
    }
}