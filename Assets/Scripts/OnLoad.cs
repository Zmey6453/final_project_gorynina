﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnLoad : MonoBehaviour
{
    public Vector3 end = new Vector3(-20.21f, 54.29f, 48.98f);
    public Vector3 start;
    public float speed = 10.0f;

    void Start()
    {
        start = transform.position;
    }


    void Update()
    {
        //transform.position = Vector3.Lerp(start, end, speed * Time.deltaTime );
        //transform.position = 
        transform.position = Vector3.Lerp(start, end, speed * Time.deltaTime);
        //start = transform.position;
    }
}
