﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
     float speed = 20.0f;
    public float speedUD = 30.0f;
    public float g = 9.8f;
    public Canvas canvas;
    public bool nearObjectFront = false;
    public bool nearObjectLeft = false;
    public bool nearObjectRight = false;
    public bool nearObjectDown = false;
    public bool nearObjectBack = false;
    Rigidbody rigidbody;//что не так?
    ButtonInd ButtonUp;
    ButtonInd ButtonDown;
    public Camera camera;
    //ButtonInd ButtonRight;
    //ButtonInd ButtonLeft;
    //ButtonInd ButtonForward;
    public GameObject Predator;
    RimpsPillar Pillar;
    Player self;
    private void Start()
    {
        ButtonUp = canvas.transform.Find("ButtonUp").GetComponent<ButtonInd>();
        ButtonDown = canvas.transform.Find("ButtonDown").GetComponent<ButtonInd>();
        self = GetComponent<Player>();
        rigidbody = self.GetComponent<Rigidbody>();
        ReverseGravity(true);
        Pillar=  GetComponent<RimpsPillar>();
    }
    void Update()
    {
        MovingVertical();
        if (transform.position.y <= -50.0f)
        {
            Pillar.wasEaten = true;
            //Predator.transform.position = new Vector3(transform.position.x, -100.0f, Predator.transform.position.z);//что за дела такие?
        }
    }
    void MovingVertical()
    {
        if (ButtonUp.isPressed)
        {
            transform.position += Vector3.up * speedUD * Time.deltaTime;
        }
        else if (ButtonDown.isPressed && !nearObjectDown)
        {
            transform.position += Vector3.down * speedUD * Time.deltaTime;       
        }
        else if (!nearObjectDown){
            transform.position += Vector3.down * g * Time.deltaTime;
            g += Time.deltaTime;
        }

        

    }
    public void MovingGorizontal(Vector3 dest)
    {
        if ((nearObjectLeft && dest.x < 0.0f) || (nearObjectRight && dest.x > 0.0f)) dest.x = 0.0f;
        if ((nearObjectBack && dest.z < 0.0f) || (nearObjectFront && dest.z > 0.0f)) dest.z = 0.0f;
        transform.Translate(dest * speed * Time.deltaTime);
    }

    void ReverseGravity(bool f)
    {
        if (f)
        {
            rigidbody.useGravity = false;
            rigidbody.isKinematic = true;           
        }
        else
        {
            rigidbody.useGravity = true;
            rigidbody.isKinematic = false;        
        }
    }
}
