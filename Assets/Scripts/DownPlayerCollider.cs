﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DownPlayerCollider : MonoBehaviour
{
    public Player player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Untagged")) player.nearObjectDown = true;
    }
    //void OnTriggerStay(Collider other)
    //{
    //    if (other.gameObject.CompareTag("Untagged")) player.nearObjectDown = true;
    //}
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Untagged")) player.nearObjectDown = false;
    }
}
