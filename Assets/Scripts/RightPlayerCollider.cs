﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightPlayerCollider : MonoBehaviour
{
    public Player player;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Untagged")) player.nearObjectRight = true;
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Untagged")) player.nearObjectRight = false;
    }
}
